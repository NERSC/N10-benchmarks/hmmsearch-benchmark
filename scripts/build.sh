#!/bin/sh

#this script should be run from the hmmsearch-benchmark directory, not scripts directory
HMMER_ROOT=$PWD

# exit on error
set -e


# download and upack base HMMER
hmmer_tar=hmmer-3.3.2.tar.gz
hmmer_dir=hmmer-3.3.2
if [[ -f "$hmmer_tar" ]]; then
    echo "file $hmmer_tar in place; skipping download"
else
    wget http://eddylab.org/software/hmmer/hmmer-3.3.2.tar.gz
fi
if [[ -d "$hmmer_dir" ]]; then
    echo "directory $hmmer_tar exists; skipping untar"
else
    tar -xvf hmmer-3.3.2.tar.gz
fi

# configure, build, test hmmer
cd hmmer-3.3.2
./configure && make && make check

# set update_hpcsrc to 1 to download the HPC file hpc_hmmsearch,
# otherwise use the existing hpc_hmmsearch file included with this repo. 
# default [0] is to use the hpc_hmmsearch contained in this repo.
update_hpcsrc=0 
if [[ $update_hpcsrc -eq 1 ]]; then
    cd $HMMER_ROOT/hpc_hmmsearch
    wget https://raw.githubusercontent.com/Larofeticus/hpc_hmmsearch/master/hpc_hmmsearch.c
fi
    
# move into hmmer source dir and compile hpc_hmmsearch file
cd $HMMER_ROOT/$hmmer_dir/src
ln -s $HMMER_ROOT/hpc_hmmsearch/Makefile.hpc .
ln -s $HMMER_ROOT/hpc_hmmsearch/hpc_hmmsearch.c .
make -f Makefile.hpc


