#!/bin/sh

#this script should be run from the hmmsearch-benchmark directory, not scripts directory
HMMER_ROOT=$PWD

# exit on error
set -e

if [[ ! -d data ]]; then
    mkdir data
fi
cd data



# download swissprot
# ftp://ftp.ebi.ac.uk/pub/databases/uniprot/previous_releases/release-2020_01/knowledgebase/RELEASE.metalink
# f384d146d9bb0b30ae447db005f5c63f  uniprot_sprot-only2020_01.tar.gz
uniprot_file=uniprot_sprot.fasta
uniprot_checksum="f384d146d9bb0b30ae447db005f5c63f  uniprot_sprot-only2020_01.tar.gz"
if [[ -f "$uniprot_file" ]]; then
    echo "file $uniprot_file in place; skipping download"
else
    wget ftp://ftp.ebi.ac.uk/pub/databases/uniprot/previous_releases/release-2020_01/knowledgebase/uniprot_sprot-only2020_01.tar.gz && \
    up_dl_chk=$(md5sum uniprot_sprot-only2020_01.tar.gz)
    if [[ "$uniprot_checksum" != "$up_dl_chk" ]]; then
        echo "uniprot database checksum failed; exiting"
        exit 1
    fi
    tar -xf uniprot_sprot-only2020_01.tar.gz && \
    rm uniprot_sprot.dat.gz uniprot_sprot_varsplic.fasta.gz uniprot_sprot.xml.gz && \
    gunzip uniprot_sprot.fasta.gz
fi

# download pfam
# ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam33.0/md5_checksums
# a901c58ba7fe26f8db276a5f03e544ea  Pfam-A.hmm.gz
pfam_file=Pfam-A.hmm
pfam_checksum="a901c58ba7fe26f8db276a5f03e544ea  Pfam-A.hmm.gz"
if [[ -f "$pfam_file" ]]; then
    echo "file $pfam_file in place; skipping download"
else
    wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam33.0/Pfam-A.hmm.gz && \
    pf_dl_chk=$(md5sum Pfam-A.hmm.gz)
    if [[ "$pfam_checksum" != "$pf_dl_chk" ]]; then
        echo "pfam database checksum failed; exiting"
        exit 1
    fi
    gunzip Pfam-A.hmm.gz
fi

output_file=ref_outut_single.txt
output_checksum="639ca62198e5013333a89aae3792f0d8  ref_output_single.txt"
if [[ -f "$output_file" ]]; then
    echo "file $output_file in place; skipping download"
else
    wget https://portal.nersc.gov/project/m888/nersc10/benchmark_data/hmmsearch/ref_output_single.txt && \
    of_dl_chk=$(md5sum ref_output_single.txt)
    if [[ "$output_checksum" != "$of_dl_chk" ]]; then
        echo "reference output checksum failed; exiting"
        rm ref_output_single.txt
	exit 1
    fi
fi


