#!/usr/bin/env python3
#Usage: validate.py <reference_output> <output>

import sys
from math import log10, floor

def validate_main():
    
    ref_Fname = sys.argv[1]
    ref_F = open( ref_Fname, 'r' )
    ref_queries = index_queries( ref_F, True )
    ref_F.close()

    sample_Fname = sys.argv[2]
    sample_F = open( sample_Fname, 'r' )
    sample_queries = index_queries( sample_F, False )
    sample_F.close()

    error_distribution = compare_queries( ref_queries, sample_queries )
    status_int = error_distribution.summarize()

    status_str = "PASSED" if (status_int==0) else "FAILED"
    print( "Validation:", status_str )

    return status_int
#validate_main


class hmm_error_distribution( dict ):

    def __init__( self ):
        self.max_error = 10
        self.missing_query_count = 0
        col_names = [ 'full E-value', 'best E-value', 'c-Evalue', 'i-Evalue' ] 
        for k in col_names:
            self[k] = [ 0 for i in range( self.max_error+2 ) ]
    #end __init__

    def increment( self, R ):
        self.missing_query_count += R.missing_query_count
        for k in self:
            self[k] = [ Ki + Ri for Ki,Ri in zip( self[k], R[k] ) ]
        return self
    #end increment

    def increment_i( self, k, i ):
        self[k][ min(i,self.max_error) ] += 1
    #end increment_i
        
    def total( self ):
        T = [ 0 for i in range( self.max_error+2 ) ]
        for k in self:
            T = [ Ti + Ki for Ti,Ki in zip( T, self[k]) ]
        return T
    #end total
        
    def summarize( self ):

        T = self.total()
        error_tol = 1.0e-4
        error_avg = ( sum( [ i * Ti for i,Ti in enumerate( T[:-1] ) ] ) * 0.1 /
                      sum( [     Ti for   Ti in            T[:-1]   ] ) )
        missing_tol = 0
        missing_num = self.missing_query_count

        status = 0
        if( error_avg > error_tol ): status=1
        if( self.missing_query_count > 0 ): status=2
        
        if( verbosity >= 1 ):
            metric_str = "{:<12} {:>9} {:>9} {:>9} {:>9} {:>9} {:>9}"
            print("============ HMMsearch Error Distribution Summary =============")
            print("---Metric--- ----------------- Mantissa Error -----------------")
            print("------------ ----0---- ----1----  ----2---- ----3---- --->=4-- ---UFL--- -Missing-")
            for k in self.keys():
                #print( metric_str.format( k, *(self[k]) ) )
                print( metric_str.format( k,
                                          self[k][0], self[k][1], self[k][2], self[k][3],
                                          sum( self[k][4:-1] ), self[k][-1] ) 
                       + "{:>9}".format("N/A") )
                print( metric_str.format( "Total",
                                          T[0], T[1], T[2], T[3],
                                          sum( T[4:-1] ), T[-1] ) 
                       +  "{:>9}".format(self.missing_query_count) )


        if( verbosity >= 0 ):
            print("Missing Query Count: {:}    Allowed: {:}".format( missing_num, missing_tol ) )
            print("Average Mantissa Error: {:.2e}    Tolerance: {:.2e}".format( error_avg, error_tol ) ) 
        
        return status

    #end summarize

#end class hmm_error_distribution
    
class hmm_query:

    #an initialized hmm_query will contain:
    #Qname : the name of the query
    #scores: { sequence_name : split( score_line ) }
    #annots: { sequence_name : split( annotation_line ) }
    
    def __init__(self, Qname, F, stop_at_threshold ):

        self.Qname = Qname
        
        scores_i = 7
        self.scores = {}
        for L in F[ scores_i : ]:
            if( L=='\n' ): break
            if( '-- inclusion threshold --' in L):
                if( stop_at_threshold ): break
                else: continue
            S = L.split()
            self.scores[ S[8] ] = S

        seq = None
        annot = None
        annots_i = F.index("Domain annotation for each sequence:\n") + 1
        self.annots = {}
        for L in F[ annots_i : ]:
            if( L=="Internal pipeline statistics summary:\n" ):
                break
            elif( L[:2]=='>>' ):
                seq=L.split()[1]
                if( stop_at_threshold and
                    seq not in self.scores ):
                    break
            elif( L[3:19]=='#    score  bias'):
                pass
            elif( L[3:19]=='-   ------ -----'):
                pass
            elif( L[3:19]=='[No individual d'):
                  pass
            elif( L=='\n' ):
                if( seq!=None and annot!=None ):
                    self.annots[seq] = annot
                seq==None
                annot=None
            else:
                if( annot==None ):
                    annot = L.split()
                else:
                    Ascore = float( annot[2] )
                    Lscore = float( L.split()[2] )
                    if Lscore > Ascore:
                        annot = L.split()                
    #__init__
                         
    def compare( self, X ):

        error_X = hmm_error_distribution()

        error_X.increment( self.compare_scores( X ) )

        error_X.increment( self.compare_annots( X ) )
        
        return error_X
    #compare
    
    def compare_scores( self, X ):

        #these are the columns that will be compared
        col_names = [ 'full E-value', 'best E-value' ]
        col_index = [ 0,              3              ]
        error_X = hmm_error_distribution()
        
        for seq in self.scores:

            if( seq not in X.scores ):
                error_X.missing_query_count += 1
                if( verbosity >= 2 ):
                    str = "Error: could not find (query,sequence): ({:},{:})"
                    print( str.format( self.Qname, seq ) )
                continue

            for i, N in zip( col_index, col_names ):

                error_i = mantissa_error( self.scores[seq][i], X.scores[seq][i] )
                error_X.increment_i( N, error_i )

                if( error_i != 0 and verbosity >=3 ):
                    str="Error for query ({:}) scores: sequence ({:})."
                    print( str.format( self.Qname, seq ) )
                    str="Column ({:})  Expected {:}   Found {:}   Error {:}"
                    print( str.format( N, self.scores[seq][i], X.scores[seq][i], error_i ) )

            #end for columns
        #end for seq in scores

        return error_X
    #compare_scores

    def compare_annots( self, X ):

        #these are the columns that will be compared
        col_names = [ 'c-Evalue', 'i-Evalue' ]
        col_index = [ 4,          5          ]
        error_X = hmm_error_distribution()

        for seq in self.annots:

            if( seq not in X.annots ):
                error_X.missing_query_count += 1
                if( verbosity >= 2 ):
                    str = "Error: could not find (query,sequence): ({:},{:})"
                    print( str.format( self.Qname, seq ) )
                continue
            
            for i, N in zip( col_index, col_names ):

                error_i = mantissa_error( self.annots[seq][i], X.annots[seq][i] )
                error_X.increment_i( N, error_i )

                if( error_i !=0 and verbosity >=3 ):
                    str="Error for query ({:}) annotations: sequence ({:})."
                    print( str.format( self.Qname, seq ) )
                    str="Column ({:})  Expected {:}   Found {:}   Error {:}"
                    print( str.format( N, self.annots[seq][i], X.annots[seq][i], error_i ) )

            #end for columns
        #end for seq
        
        return error_X
    #compare_annots
    
    foo=0
#end hmm_query


def index_queries( F, stop_at_threshold ):

    I = {}
    FL = F.readlines()
    for iL, L in enumerate(FL):

        if( L[0:6] == "Query:" ):
            Qname = L[6:].strip()
            Qi = iL

        if( L[0:2] == "//" ):
            Qf = iL
            I[ Qname ] = hmm_query( Qname, FL[Qi:Qf], stop_at_threshold )

    return I
#index_queries
          
def compare_queries( ref, X ):

    errorX = hmm_error_distribution()

    for Q in ref:

        if ( Q not in X ):
            errorX.missing_query_count += 1
            if( verbosity >= 2 ):
                str = "Error: Could not find query {:} in tested output"
                print( str.format( Q ) )
            continue

        errorQ = ref[Q].compare( X[Q] )

        errorX.increment( errorQ )
          
    #end for Q in ref

    return errorX
#compare_queries

def mantissa_split( s ):

    #values less than 1e-280 are indistinguishable from zero
    #this threshold (280) is somewhat arbitrary
    if( s=='0' ):
        return 1.0, -280
    
    try:
        f = float(s)
        x = floor( log10( f ) )
        m = f / pow( 10.0, x ) 
    except:
        print("Error decomposing Evalue:", s )
        exit(1)            
        
    return m, x    
#end mantissa_split
            
def mantissa_error( expected_str, measured_str ):
    
    #returns first digit of the abs difference between the base-10 mantissa 
    
    mantissa_x, exponent_x = mantissa_split( expected_str )
    mantissa_m, exponent_m = mantissa_split( measured_str )
    
    #convert both numbers to match the less-negative exponent
    if( exponent_m > exponent_x ):
        mantissa_x *= pow( 10.0, exponent_x - exponent_m )
        exponent_x = exponent_m
    if( exponent_x > exponent_m ):
        mantissa_m *= pow( 10.0, exponent_m - exponent_x )
        exponent_m = exponent_x

    #values less than 1e-280 are indistinguishable from zero
    #this threshold (280) is somewhat arbitrary
    if( exponent_m <= -280 ): return 0

    #compute the first digit of the difference between the mantissas
    #multiply by 10 to shift from tenths to ones
    mantissa_d = round( abs( mantissa_x - mantissa_m ) * 10 )
        
    return mantissa_d
#mantissa_error

#verbosity levels:
# [ 0:summary_only,
#   1:error distribution,
#   2:list_missing_queries,
#   3:list_evalue_errors ]
verbosity=0
validate_main()
