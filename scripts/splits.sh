#!/bin/bash
#extracts the first few sequences from a fast or hmm file
#arg1: dbfile - name of file to be split
#arg2: split_size - number of sequences in the split
#output will be in the file dbfile-split<n_seq>.[fasta,hmm]


dbfile=$1
split_size=$2

dbtype=unknown
for tstr in fasta hmm; do
    t=`echo $dbfile | grep -c $tstr`
    if [[ $t -eq 1 ]]; then dbtype=$tstr; fi
done


case "$dbtype" in
  fasta)
    seqstr=">"
    ;;
  
  hmm)
    seqstr="HMMER3"
    ;;
  
  *)
    echo "Cannot recognize DB type. Expected one of [ swisprot, hmm ]. Found $1."
    exit 1
esac

split_fname=`basename $dbfile | sed s/\.$dbtype/-${split_size}.$dbtype/`

awkcmd="BEGIN {i_seq=0; n_seq=$split_size; file=\"$split_fname\"; } \
        /^$seqstr/ { if(i_seq==n_seq){exit;} print >> file; i_seq++; next; } \
        { print >> file; }" 

#echo awk \"$awkcmd\" "$dbfile"

awk "$awkcmd" < $dbfile
