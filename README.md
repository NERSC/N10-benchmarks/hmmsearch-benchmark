This repository describes the Metagenome Annotation workflow component benchmark
from the [NERSC-10 Benchmark Suite](https://www.nersc.gov/systems/nersc-10/benchmarks).<br>
The [NERSC-10 benchmark run rules](https://gitlab.com/NERSC/N10-benchmarks/run-rules-and-ssi/-/blob/main/N10_Benchmark_RunRules.pdf)
should be reviewed before running the benchmark.<br>
Note, in particular:
- The NERSC-10 run rules apply to this benchmark except where explicitly noted within this README.
- The run rules define "baseline", "ported" and "optimized" categories of performance optimization.
- Responses to the NERSC-10 RFP should include performance estimates for the "baseline" category;
  results for the "ported" and "optimized" categories are optional.
- The projected walltime for the target problem on the target system 
  must not exceed the reference time measured by running the reference problem on Perlmutter.
- The "capability factor" (c) descibes the increase in
computational work (e.g. flops) between the reference and target problem sizes.
The capability factor is also used  to compute  the
[Sustained System Improvement (SSI) metric](https://gitlab.com/NERSC/N10-benchmarks/run-rules-and-ssi/-/blob/main/Workflow_SSI.pdf ).


# 0. Metagenome Annotation Overview

This  benchmark is based on the computation workflow of the
Joint Genome Institute's (JGI)'s Integrated Microbial Genomes project (IMG).
IMG's scientific objective is to analyze, annotate and distribute 
microbial genomes and microbiome datasets. 
Thousands of genomes are processed annually.

The production IMG workflow begins with collection of DNA sequences in the laboratory.
The data is then transferred to NERSC, and is processed in several multi-step computational phases:
- Quality Assurance (QA) and Assembly
- Structural Annotation
- Functional Annotation
- Binning

These phases are described in greater detail [here](https://pubmed.ncbi.nlm.nih.gov/34006627/).
Within the functional annotation phase,
the `hmmsearch` program uses the majority of computational cycles
and is the basis for this benchmark. 
Other phases, most notably the Assembly phase,
have also been targetted for exa-scale HPC
through the [ECP Exabiome project](https://www.exascaleproject.org/research-project/exabiome/).

`hmmsearch` is one program within the [HMMER biosequence analysis package](http:hmmer.org) 
that uses a profile Hidden Markov Models (HMM) algorithm to perform a search for
matches between input sequences and a reference database of sequence profiles.
Comparisons are performed using a modified Viterbi algorithm (a form of dynamic programming).
This implementation is written in the C language, and is a CPU-only code.
The HMMER distribution of hmmsearch uses OpenMP for shared-memory parallelism,
but does not provide a mechanism for distributed-memory parallelism. 
In practice, collections of independent hmmsearch jobs run across multiple nodes in a data-parallel fashion. 


# 1. `hmmsearch` Code Access and Compilation Details

The process of building hpc_hmmsearch has three basic steps:
- obtaining the base version of HMMER
- compiling the base version of HMMER
- compiling the HPC version of hmmsearch

The `scripts/build.sh` script performs all of these steps,
which are described in detail below.

## 1.1 Obtaining HMMER base source code

HMMER can be downloaded and unpacked using the commands:
```
wget http://eddylab.org/software/hmmer/hmmer-3.3.2.tar.gz
tar -xvf hmmer-3.3.2.tar.gz
```

## 1.2 Compiling the base version of HMMER
The base version of HMMER can be compiled using the standard `configure` and `make` procedure.
```
cd hmmer-3.3.2
./configure
make
make check
```

## 1.3 Compiling the HPC version of hmmsearch

Because `hpc_hmmsearch` is an external addition to HMMER, it must be compiled subsequent to the base HMMER install.
NERSC maintains a HPC-specific version of hmmsearch 
which uses OpenMP tasks for more efficient thread parallelism
and increased overlap between I/O and computation.
The HPC optimizations are described in [this paper](https://ieeexplore.ieee.org/document/8425415).
This additional HPC-specific source file is hosted at
https://github.com/Larofeticus/hpc_hmmsearch.
For convenience, a copy of hpc_hmmsearch is included in this repository in the `hpc_hmmsearch` directory.
The `hpc_hmmsearch.c` file should be added to the HMMER base directory's `src` folder before recompiling with `make`.
```
cp hpc_hmmsearch/hpc_hmmsearch.c hmmer-3.3.2/src
cp hpc_hmmsearch/Makefile.hpc    hmmer-3.3.2/src
cd hmmer-3.3.2/src
make -f Makefile.hpc
```


# 2. Data Access

Two data sets are needed to run the benchmark:
 * SwissProt protein sequence database
 * Pfam HMM profile database
 * HMMsearch sample output (for validation tests)

The script, `scripts/wget_data.sh`, downloads and unpacks these files into the `data` directory.
This script should only need to be run once from the `hmmsearch-benchmark` directory.

| | Download Information |
|-|-|
| **SwissProt** | Version: January 2020 <br> File name: uniprot_sprot-only2020_01.tar.gz <br> Download size: 1.3 GB <br> md5checksum: f384d146d9bb0b30ae447db005f5c63f |
| **Pfam HMM**  | Version: 33.0         <br> File name: Pfam-A.hmm.gz                    <br> Download size: 260 MB <br> md5checksum: a901c58ba7fe26f8db276a5f03e544ea |
| **HMMsearch output** |                <br> File name: ref_output_single.txt            <br> Download size: 1.4 GB <br> md5checksum: 639ca62198e5013333a89aae3792f0d8 |

# 3. Running the Benchmark

The `benchmark` directory contains run scripts for four problem sizes:
"tiny", "single-task", "reference" and "target".
The "tiny" and "single-task" job scripts are provided only to facilitate profiling.

- `run_hmm_tiny.sub` compares small slices of the SwissProt and Pfam files to each other;
it is provided to enable profiling of key kernels on a single CPU core.

- `run_hmm_single.sub` compares the full SwissProt and Pfam files;
it is provided for profiling HMMsearch on a single node (or numa-node).
HMMsearch (as currently written) uses OpenMP for shared-memory parallelism
and does not have a distributed-memory (e.g. MPI) level of parallelism,
so the "single-task" problem cannot strong-scale beyond a single node.

- `run_hmm_reference.sub` emulates the use of many HMMsearch jobs 
to compare a collection of protein sequence databases to a collection of HMM files.
The benchmark simplifies this workflow by using the same database and HMM file for each HMMsearch job.
The output of HMMsearch jobs performed by `run_hmm_reference.sub` is intentionally  redundant; their output is identical. 
In the absence of filesystem, network or other sources of contention, 
the runtimes of `run_hmm_single.sub` and `run_hmm_reference.sub` should be the same.

- `run_hmm_target.sub` has the same functionality as `run_hmm_reference.sub`.
The reference and target problems for are the same.
However, for the target problem, the number of hpc_hmmsearch tasks started (NTASKS) is 10x larger than the reference.

The resources used to run these problems on Perlmutter are listed in the following table.
Each Perlmutter CPU (PM-CPU) node has two sockets, 
each with a 64-core AMD EPYC 7763 processor.
For the single and reference runs, one HMMsearch job used 64 OpenMP threads on one EPYC processor.

| Size         | Capability<br> Factor (c) | Concurrency       | Memory<br>per task (GB) | Walltime<br>(sec) |
| :---:        |:---:| :---:             | :---: | :---:  |
|   tiny       | N/A |   1 PM-CPU core   | 0.5   |   51   |
| single-task  |1/640|   1 PM-CPU socket |  31   |  600   |
|  reference   |  1  | 320 PM-CPU nodes  |  31   |  615*  |
|  target      | 10  |                   |       |        |

The capability factor (c) listed in the table describes
the amount of work performed relative to the reference problem.
The reference time from NERSC's Perlmutter system was evaluated
using 320 CPU-nodes and is marked by a *.


## 3.1 hmmsearch run rules.

The aim of the hmmsearch benchmark is to measure the aggregate throughput of concurrent `hpc_hmmsearch` jobs
as illustrated in `run_hmm_target.sh`.
The following changes to `run_hmm_target.sh` are allowed for baseline, ported and optimized runs.
- The data files (`uniprot_sprot.fasta` and `Pfam-A.hmm`) may be replicated before running;
this may improve performance by avoiding file-system contention when running multiple tasks run concurrently.
See `run_hmm_replicated.sh` for an example.
Note that the file replication time is not included in the HMMsearch_walltime. 
- Prior to running, data and executables may be staged to intermediate storage tiers, but not directly to the compute nodes.
- The directory structure for storing input and output files may be modified (e.g. single directory vs directory per task).
- The job launch command (i.e. ```srun --multi-prog```) may be replaced 
  by other job management tools (e.g. [GNU-parallel](https://www.gnu.org/software/parallel/) )
- The `cleanup` option at the end of the script may be enabled to remove duplicate files.


# 4. Results

## 4.1 Validation

Correctness can be verified by comparing the output files to the reference output.
Any output file ("out.txt" in the `run_hmm_<size>.sh` scripts)
should match the reference output (`ref_output_<size>.txt`)
with only a modest number minor numerical differences
due to the code's relatively high sensitivity to roundoff errors 
However, the output may be ordered differently despite being equivalent.
The `scripts/validate.py` should be used to
compare results to the reference output.
This script accounts for output reordering
and estimates the probability that any individual comparison 
was incorrectly labeled as a weak or strong match. 
For correct output, this probability must be less than 2.00e-4.
Last, it prints a simple indication of the result.
```
# for the tiny problem:
> scripts/validate.py data/ref_output_tiny.txt   benchmark/<job_dir>/out.txt
| Missing Query Count: 0    Allowed: 0
| Average Mantissa Error: 4.18e-05    Tolerance: 2.00e-04
| Validation: PASSED
#
# for any other problem:
> scripts/validate.py data/ref_output_single.txt benchmark/<job_dir>/out.txt
| Missing Query Count: 0    Allowed: 0
| Average Mantissa Error: 3.25e-06    Tolerance: 2.00e-04
| Validation: PASSED
```

## 4.2 Timing
HMMsearch_walltime is determined from the difference between calls to `date` before and after the job,
and is clearly labeled in the job's standard output.

## 4.3 Reporting

Benchmark results should include projections of the HMMsearch_walltime for the "target" problem.
The hardware configuration 
(i.e. the number of elements from each pool of computational resources) 
needed to achieve the estimated timings must also be provided. 
For example, if proposing a system with more than one type of compute node, 
then report the number and type of nodes used to run the workflow components. 
If the proposed system enables disaggregation/ composability, 
a finer grained resource list is needed, as described in the
[Workflow-SSI document](https://gitlab.com/NERSC/N10-benchmarks/run-rules-and-ssi/-/blob/main/Workflow_SSI.pdf).

For the electronic submission,
include all the source and makefiles used to build on the target platform
and input files and run-scripts.
Include all standard output files.
