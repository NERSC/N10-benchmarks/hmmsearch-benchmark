The `hmmsearch-benchmark/data`
holds the files needed to run and validate the benchmark:
- Pfam-A.hmm
- uniprot_sprot.fasta
- ref_output_tiny.txt
- ref_output_single.txt

Several of these files must be downloaded separately
due to their large sizes (up to 1.4 GB).
Please use `hmmsearch-benchmark/scripts/wget-data.sh`
to obtain them.