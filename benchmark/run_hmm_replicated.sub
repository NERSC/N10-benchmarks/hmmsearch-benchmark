#!/bin/bash
##SBATCH -N 320
#SBATCH -N 2
#SBATCH -C cpu
#SBATCH --exclusive
#SBATCH -t 20:00
#SBATCH -J hmmbench-replicated
#SBATCH -A m888
#SBATCH -q regular
#SBATCH -o replicated-%j.out

module list

HMM_BENCH=../..
HMM_DATA=$HMM_BENCH/data
HMM_SEARCH=$HMM_BENCH/hmmer-3.3.2/src/hpc_hmmsearch

TASKS_PER_NODE=2
NTASKS=$(( SLURM_JOB_NUM_NODES * TASKS_PER_NODE ))
NTASKS1=$((NTASKS-1))
NCPU=64
NHWT=128

mkdir replicated-$SLURM_JOB_ID ; cd replicated-$SLURM_JOB_ID
cp ${0} slurm_script
ln -s $HMM_DATA .
for (( i=0; i<NTASKS; i++ )); do
    cp data/Pfam-A.hmm Pfam-A.hmm-$i
    cp data/uniprot_sprot.fasta uniprot_sprot.fasta-$i
done

#using slurm's MPMD features to launch concurrent hpc_hmmsearch tasks
#slurm will replace %o with the task-id
#so that the output of each task will go to its own file: out-%0.txt
HMM_CMD="$HMM_SEARCH \
       --cpu ${NCPU} \
       -o out-%o.txt \
       --noali \
       Pfam-A.hmm-%o \
       uniprot_sprot.fasta-%o"

echo "0-${NTASKS1} $HMM_CMD" > replicated.conf

date_i=$(date '+%s')

srun \
    -N ${SLURM_JOB_NUM_NODES} \
    -n ${NTASKS} \
    -c ${NHWT} \
    --multi-prog replicated.conf

date_f=$(date '+%s')


walltime=$(( date_f - date_i ))
echo "HMMsearch_walltime: $walltime"
$HMM_BENCH/scripts/validate.py $HMM_BENCH/data/ref_output_single.txt out-0.txt

#clean up the output files, saving only the 0th
cleanup=0
if [ $cleanup -eq 1 ]; then
  mv out-0.txt tmp-out-0.txt
  rm out-*.txt
  mv tmp-out-0.txt out-0.txt
  rm Pfam-A.hmm-*
  rm uniprot_sprot.fasta-*
fi
